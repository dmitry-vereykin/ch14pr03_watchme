/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class WatchMe extends JApplet {
        private int leftEyeX = 30;
        private int leftEyeY = 40;
        private int rightEyeX = 90;
        private int rightEyeY = 40;
        private int leftPupilX = leftEyeX + 10;
        private int leftPupilY = leftEyeY + 20;
        private int rightPupilX = rightEyeX + 10;
        private int rightPupilY = rightEyeY + 20;
        boolean exited = false;
        boolean up = false;
        boolean down = false;
        boolean left = false;
        boolean right = false;
        boolean straight = false;

    public void init() {
        getContentPane().setBackground(Color.white);
        addMouseListener(new MyMouseListener());
        addMouseMotionListener(new MyMouseMotionListener());
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Color.black);
        g.drawOval(leftEyeX, leftEyeY, 40, 80);
        g.drawOval(rightEyeX, rightEyeY, 40, 80);


        if (exited)
            drawPupils(g, 0, 0);
        else {
            if (up)
                drawPupils(g, 0, -20);
            else if (down)
                drawPupils(g, 0, 25);
            else if (left)
                drawPupils(g, -10, 0);
            else if (right)
                drawPupils(g, 10, 0);
            else if (straight)
                drawPupils(g, 0, 0);
            else
                drawPupils(g, 0, 0);
        }

    }

    private class MyMouseListener extends MouseAdapter {
        public void mouseEntered(MouseEvent e) {
            exited = false;
            repaint();
        }

        public void mouseExited(MouseEvent e) {
            leftPupilX = leftEyeX + 10;
            leftPupilY = leftEyeY + 20;
            rightPupilX = rightEyeX + 10;
            rightPupilY = rightEyeY + 20;
            exited = true;
            straight = true;
            repaint();
        }
    }

    private class MyMouseMotionListener implements MouseMotionListener {

        public void mouseDragged(MouseEvent e) {
        }

        public void mouseMoved(MouseEvent e) {

            if(!exited) {
                if (e.getY() < 30) {
                    up = true;
                    down = false;
                    left = false;
                    right = false;
                    straight = false;
                } else if (e.getY() > leftEyeY + 80) {
                    up = false;
                    down = true;
                    left = false;
                    right = false;
                    straight = false;
                } else if (e.getX() < leftEyeX) {
                    up = false;
                    down = false;
                    left = true;
                    right = false;
                    straight = false;
                } else if (e.getX() > rightEyeX + 40) {
                    up = false;
                    down = false;
                    left = false;
                    right = true;
                    straight = false;
                } else {
                    up = false;
                    down = false;
                    left = false;
                    right = false;
                    straight = true;
                }
            } else {
                up = false;
                down = false;
                left = false;
                right = false;
                straight = true;
            }
            repaint();
        }
    }

    public void drawPupils(Graphics g, int x, int y) {
        g.fillOval(leftPupilX + x, leftPupilY + y, 20, 30);
        g.fillOval(rightPupilX + x, rightPupilY + y, 20, 30);
    }

}
